variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "btwiusearch9102@outlook.com"
}

variable "db_username" {
  description = "username for the RDS postgress instance "
}

variable "db_password" {
  description = "password for the RDS postgress instance "
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "386897818119.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for Proxy"
  default     = "386897818119.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "secret key for django"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "it39.academy"
}

variable "subdomain" {
  type = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}
